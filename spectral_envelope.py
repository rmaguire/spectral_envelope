from __future__ import print_function, division
import obspy
import matplotlib
import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from obspy import UTCDateTime
from obspy.signal.util import next_pow_2
from pylab import get_current_fig_manager

matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42
fm = get_current_fig_manager()
fm.show()

#------------------------------------------------------------------
# Set Parameters
#------------------------------------------------------------------
data_dir = '/Users/rmaguire/Desktop/Treated/HIGH_FREQUENCY/B/S0231b' #directory with .SAC files
dirty_glitch_remover = True
vmin = -210 #dB (for colorbar)
vmax = -160 #dB (for colorbar)
units = 'acceleration'
event_name = data_dir.split('/')[-1]

#TODO      1) asthetics... draw rectangles on other spec plots etc.

def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx

class SpectrogramDisplay:
    def __init__(self,fig,spec_ax,bhz_wave_ax,bhn_wave_ax,bhe_wave_ax,bhz_p,bhn_p,bhe_p,f,t,st):
        self.fig = fig
        self.spec_ax = spec_ax
        self.bhz_wave_ax = bhz_wave_ax
        self.bhn_wave_ax = bhn_wave_ax
        self.bhe_wave_ax = bhe_wave_ax
        self.cid = fig.canvas.mpl_connect('button_press_event', self)
        self.bhz_p = bhz_p
        self.bhn_p = bhn_p
        self.bhe_p = bhe_p
        self.f = f
        self.t = t
        self.nclicks = 0
        self.xdata = []
        self.ydata = []
        self.line1 = None
        self.line2 = None
        self.line3 = None
        self.line4 = None
        self.utc = st[0].stats.starttime

    def __call__(self,event):

        if self.nclicks < 2:
            self.nclicks += 1

            self.xdata.append(event.xdata)
            self.ydata.append(event.ydata)

            if self.nclicks == 1:
                self.line1 = self.spec_ax.axvline(event.xdata,color='black',linestyle='dashed', linewidth=1)
                self.line2 = self.spec_ax.axhline(event.ydata,color='black',linestyle='dashed', linewidth=1)

            elif self.nclicks == 2:
                self.line3 = self.spec_ax.axvline(event.xdata,color='black',linestyle='dashed', linewidth=1)
                self.line4 = self.spec_ax.axhline(event.ydata,color='black',linestyle='dashed', linewidth=1)

            if self.nclicks == 2:
                xi_0 = find_nearest(self.t,self.xdata[0])
                xi_1 = find_nearest(self.t,self.xdata[1])
                yi_0 = find_nearest(self.f,self.ydata[0])
                yi_1 = find_nearest(self.f,self.ydata[1])

                bhz_sub_array = self.bhz_p[yi_0:yi_1,xi_0:xi_1]
                bhn_sub_array = self.bhn_p[yi_0:yi_1,xi_0:xi_1]
                bhe_sub_array = self.bhe_p[yi_0:yi_1,xi_0:xi_1]

                twindow_start = self.t[xi_0]
                twindow_end = self.t[xi_1]
                utcwindow_start = self.utc + twindow_start
                utcwindow_end = self.utc + twindow_end
                fwindow_start = self.f[yi_0]
                fwindow_end = self.f[yi_1]

                bhz_envelope = np.average(bhz_sub_array,axis=0)
                max_env_bhz = np.max(bhz_envelope)
                bhz_envelope /= max_env_bhz
                t_wave = np.linspace(twindow_start,twindow_end,len(bhz_envelope))
                self.bhz_wave_ax.plot(t_wave,bhz_envelope)
                self.bhz_wave_ax.set_xlim(np.min(t_wave),np.max(t_wave))

                bhn_envelope = np.average(bhn_sub_array,axis=0)
                max_env_bhn = np.max(bhn_envelope)
                bhn_envelope /= max_env_bhn
                self.bhn_wave_ax.plot(t_wave,bhn_envelope)
                self.bhn_wave_ax.set_xlim(np.min(t_wave),np.max(t_wave))

                bhe_envelope = np.average(bhe_sub_array,axis=0)
                max_env_bhe = np.max(bhe_envelope)
                bhe_envelope /= max_env_bhe
                self.bhe_wave_ax.plot(t_wave,bhe_envelope)
                self.bhe_wave_ax.set_xlim(np.min(t_wave),np.max(t_wave))

                fout1 = open('{}_bhz_envelope.dat'.format(event_name),'w')
                fout2 = open('{}_bhn_envelope.dat'.format(event_name),'w')
                fout3 = open('{}_bhe_envelope.dat'.format(event_name),'w')

                fout1.write('#normalization factor {}\n'.format(max_env_bhz))
                fout1.write('#file start time {}\n'.format(self.utc))
                fout1.write('#time window {} - {}\n'.format(str(utcwindow_start),str(utcwindow_end)))
                fout1.write('#freq window {} - {} Hz\n'.format(fwindow_start,fwindow_end))
                fout2.write('#normalization factor {}\n'.format(max_env_bhn))
                fout2.write('#file start time {}\n'.format(self.utc))
                fout2.write('#time window {} - {}\n'.format(str(utcwindow_start),str(utcwindow_end)))
                fout2.write('#freq window {} - {} Hz\n'.format(fwindow_start,fwindow_end))
                fout3.write('#normalization factor {}\n'.format(max_env_bhe))
                fout3.write('#file start time {}\n'.format(self.utc))
                fout3.write('#time window {} - {}\n'.format(str(utcwindow_start),str(utcwindow_end)))
                fout3.write('#freq window {} - {} Hz\n'.format(fwindow_start,fwindow_end))

                for i in range(0,len(t_wave)):
                    fout1.write('{:10.10f} {:10.10f}\n'.format(t_wave[i],bhz_envelope[i]))
                    fout2.write('{:10.10f} {:10.10f}\n'.format(t_wave[i],bhn_envelope[i]))
                    fout3.write('{:10.10f} {:10.10f}\n'.format(t_wave[i],bhe_envelope[i]))

                fout1.close()
                fout2.close()
                fout3.close()

                #np.savetxt('{}_bhz_envelope.dat'.format(event_name),np.c_[t_wave,bhz_envelope])
                #np.savetxt('{}_bhn_envelope.dat'.format(event_name),np.c_[t_wave,bhn_envelope])
                #np.savetxt('{}_bhe_envelope.dat'.format(event_name),np.c_[t_wave,bhe_envelope])

        else:
            self.nclicks = 0
            self.xdata = []
            self.ydata = []
            self.line1.remove()
            self.line2.remove()
            self.line3.remove()
            self.line4.remove()
            self.bhz_wave_ax.clear()
            self.bhn_wave_ax.clear()
            self.bhe_wave_ax.clear()
            self.bhz_wave_ax.set_xlabel('time [s]')
            self.bhn_wave_ax.set_xlabel('time [s]')
            self.bhe_wave_ax.set_xlabel('time [s]')

        self.fig.canvas.draw()

def create_axes():

    #-----------------------------------------------------------------------
    #set sizes
    #-----------------------------------------------------------------------
    c = 0
    fig = plt.figure(figsize=[12,6])

    ax_width = 0.22
    ax_height = 0.30
    ax_wave_ystart = 0.15
    ax_spec_ystart = 0.55
    ax_xstart = 0.1
    ax_xoffset = 0.275

    #-----------------------------------------------------------------------
    #spectrogram axes
    #-----------------------------------------------------------------------
    bhz_spec_ax = fig.add_axes([ax_xstart, 
                                ax_spec_ystart,
                                ax_width,
                                ax_height])
    c+=1

    bhn_spec_ax = fig.add_axes([ax_xstart + ax_xoffset,
                                ax_spec_ystart,
                                ax_width,
                                ax_height])
    c+=1

    bhe_spec_ax = fig.add_axes([ax_xstart + 2.*ax_xoffset,
                                ax_spec_ystart,
                                ax_width,
                                ax_height])

    #-----------------------------------------------------------------------
    #waveform axes
    #-----------------------------------------------------------------------
    c = 0

    bhz_wave_ax = fig.add_axes([ax_xstart, 
                                ax_wave_ystart,
                                ax_width,
                                ax_height])
    c+=1

    bhn_wave_ax = fig.add_axes([ax_xstart + ax_xoffset,
                                ax_wave_ystart,
                                ax_width,
                                ax_height])
    c+=1

    bhe_wave_ax = fig.add_axes([ax_xstart + 2.*ax_xoffset,
                                ax_wave_ystart,
                                ax_width,
                                ax_height])

    #-----------------------------------------------------------------------
    #colorbar axes
    #-----------------------------------------------------------------------
    c = 0
    cbar_bhz = fig.add_axes([0.91,
                             0.55,
                             0.01,
                             0.3])

    return fig,bhz_spec_ax,bhn_spec_ax,bhe_spec_ax, \
           bhz_wave_ax,bhn_wave_ax,bhe_wave_ax, \
           cbar_bhz


def calc_spectrogram(trace,winlen_sec,overlap):
    winlen = int(winlen_sec*trace.stats.sampling_rate)
    Fs = trace.stats.sampling_rate
    noverlap = int(winlen*overlap)
    p,f,t = mlab.specgram(trace.data, NFFT=winlen,
                          Fs=Fs, noverlap=noverlap,
                          pad_to=next_pow_2(winlen)*4)
    return p,f,t

def main():

    #set main parameters
    winlen_sec = 50.0
    overlap = 0.9
    bp_fmin = 1/5.0
    bp_fmax = 1/3.0

    #set mdates parameters for plotting
    hours = mdates.HourLocator()
    mins = mdates.MinuteLocator(byminute=np.arange(30,60,30))
    cmap='plasma'

    #get axes
    fig, \
    bhz_spec_ax,bhn_spec_ax,bhe_spec_ax, \
    bhz_wave_ax,bhn_wave_ax,bhe_wave_ax, \
    cbar_bhz = create_axes()

    st = obspy.read(data_dir+"/*.SAC")
    if units == 'acceleration':
        st = st.differentiate() #convert to accel.

    #plot spectrograms
    #-------------------------------------------------------------
    def to_mdates(x,utc=st[0].stats.starttime):
        y = mdates.num2date(utc.matplotlib_date +x/(60.*60.*24.))
        return y
    
    def from_mdates(x,utc=st[0].stats.starttime):
        #matplotlib_dates = mdates.date2num(x)
        y = (x - utc.matplotlib_date) * (60.*60.*24.)
        #y = (utc.matplotlib_date-x) * (60.*60.*24.)
        return y

    #calculate Z spectrogram------------------------
    tr = st.select(channel='BHZ').copy()[0]
    time = np.linspace(0,tr.stats.npts*tr.stats.delta,tr.stats.npts)
    bhz_p,f,t = calc_spectrogram(tr,winlen_sec,overlap=overlap)


    #N spectrogram------------------------
    tr = st.select(channel='BHN').copy()[0]
    bhn_p,f,t = calc_spectrogram(tr,winlen_sec,overlap=overlap)

    #E spectrogram------------------------
    tr = st.select(channel='BHE').copy()[0]
    bhe_p,f,t = calc_spectrogram(tr,winlen_sec,overlap=overlap)

    #Dirty glitch remover----------------
    if dirty_glitch_remover:
        threshold = -180. #(threshold in dB)
        vals = []
        for i in range(0,bhz_p.shape[1]):
            bhz_col = 10.*np.log10(bhz_p[:,i])
            bhn_col = 10.*np.log10(bhn_p[:,i])
            bhe_col = 10.*np.log10(bhe_p[:,i])

            vals.append(np.average(bhz_col))

            if np.average(bhz_col) > threshold:
                bhz_p[:,i] = 1e-20
            if np.average(bhn_col) > threshold:
                bhn_p[:,i] = 1e-20
            if np.average(bhe_col) > threshold:
                bhe_p[:,i] = 1e-20

    #Plot spectrograms-------------------------------------------------
    bhz_spec_ax.pcolormesh(t,f,10*np.log10(bhz_p),vmin=vmin,vmax=vmax,cmap=cmap)
    bhz_spec_ax.set_rasterized(True)
    bhz_spec_ax.set_title('BHZ',loc='left',fontsize=14)
    bhz_spec_ax.set_ylim([0,3])
    #bhz_xaxis_top = bhz_spec_ax.secondary_xaxis('top', functions=(to_mdates,from_mdates))
    #bhz_xaxis_top.xaxis.set_major_locator(hours)
    #bhz_xaxis_top.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
    #bhz_xaxis_top.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d-%H:%M'))

    bhn_spec_ax.pcolormesh(t,f,10*np.log10(bhn_p),vmin=vmin,vmax=vmax,cmap=cmap)
    bhn_spec_ax.set_rasterized(True)
    bhn_spec_ax.set_title('BHN',loc='left',fontsize=14)
    bhn_spec_ax.set_ylim([0,3])

    bhe_spec_ax.pcolormesh(t,f,10*np.log10(bhe_p),vmin=vmin,vmax=vmax,cmap=cmap)
    bhe_spec_ax.set_rasterized(True)
    bhe_spec_ax.set_title('BHE',loc='left',fontsize=14)
    bhe_spec_ax.set_ylim([0,3])

    #Make interactive spectrogram display--------------------------------
    sd = SpectrogramDisplay(fig,
                            bhz_spec_ax,
                            bhz_wave_ax,
                            bhn_wave_ax,
                            bhe_wave_ax,
                            bhz_p,
                            bhn_p,
                            bhe_p,
                            f,t,st)

    #setup colorbar
    mappable = bhz_spec_ax.collections[0]
    plt.colorbar(mappable=mappable, cax=cbar_bhz,label='PSD m$^2$/s$^4$/Hz [dB]')

    #set labels
    bhz_spec_ax.set_ylabel('frequency [Hz]')
    bhz_wave_ax.set_ylabel('envelope')

    #set log axis
    bhz_spec_ax.set_yscale('log')
    bhn_spec_ax.set_yscale('log')
    bhe_spec_ax.set_yscale('log')
    bhz_spec_ax.set_ylim([5.5e-2,5])
    bhn_spec_ax.set_ylim([5.5e-2,5])
    bhe_spec_ax.set_ylim([5.5e-2,5])

    #set xlabel and ticks
    bhz_spec_ax.set_xlim([np.min(time),np.max(time)])
    bhn_spec_ax.set_xlim([np.min(time),np.max(time)])
    bhe_spec_ax.set_xlim([np.min(time),np.max(time)])
    bhz_spec_ax.set_xlabel('time [s]')
    bhn_spec_ax.set_xlabel('time [s]')
    bhe_spec_ax.set_xlabel('time [s]')
    bhz_wave_ax.set_xlabel('time [s]')
    bhn_wave_ax.set_xlabel('time [s]')
    bhe_wave_ax.set_xlabel('time [s]')

    utcdate = st[0].stats.starttime
    title = '{}, {}-{:02d}-{:02d}'.format(event_name,utcdate.year,utcdate.month,utcdate.day)
    fig.suptitle(title,fontsize=16)

    plt.show()

plt.tight_layout()
main()
