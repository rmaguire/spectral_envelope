from __future__ import print_function, division
from sys import argv
import obspy
import matplotlib
import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from obspy import UTCDateTime
from obspy.signal.util import next_pow_2
from pylab import get_current_fig_manager

matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42
fm = get_current_fig_manager()
fm.show()

#------------------------------------------------------------------
# Read command line parameters
#------------------------------------------------------------------
data_dir = argv[1] #directory with .SAC files
freqmin = np.float(argv[2])
freqmax = np.float(argv[3])
event_name = data_dir.split('/')[-1]

#------------------------------------------------------------------
# Set Program Parameters
#------------------------------------------------------------------
dirty_glitch_remover = True
vmin = -210 #dB (for colorbar)
vmax = -160 #dB (for colorbar)
units = 'acceleration'

def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx

def calc_spectrogram(trace,winlen_sec,overlap):
    winlen = int(winlen_sec*trace.stats.sampling_rate)
    Fs = trace.stats.sampling_rate
    noverlap = int(winlen*overlap)
    p,f,t = mlab.specgram(trace.data, NFFT=winlen,
                          Fs=Fs, noverlap=noverlap,
                          pad_to=next_pow_2(winlen)*4)
    return p,f,t

def main():

    print('working on event {}'.format(data_dir))

    #set main parameters
    winlen_sec = 50.0
    overlap = 0.9
    bp_fmin = 1/5.0
    bp_fmax = 1/3.0

    st = obspy.read(data_dir+"/*.SAC")
    if units == 'acceleration':
        st = st.differentiate() #convert to accel.

    #calculate Z spectrogram------------------------
    tr = st.select(channel='BHZ').copy()[0]
    time = np.linspace(0,tr.stats.npts*tr.stats.delta,tr.stats.npts)
    bhz_p,f,t = calc_spectrogram(tr,winlen_sec,overlap=overlap)


    #N spectrogram------------------------
    tr = st.select(channel='BHN').copy()[0]
    bhn_p,f,t = calc_spectrogram(tr,winlen_sec,overlap=overlap)

    #E spectrogram------------------------
    tr = st.select(channel='BHE').copy()[0]
    bhe_p,f,t = calc_spectrogram(tr,winlen_sec,overlap=overlap)

    #Dirty glitch remover----------------
    if dirty_glitch_remover:
        threshold = -180. #(threshold in dB)
        vals = []
        for i in range(0,bhz_p.shape[1]):
            bhz_col = 10.*np.log10(bhz_p[:,i])
            bhn_col = 10.*np.log10(bhn_p[:,i])
            bhe_col = 10.*np.log10(bhe_p[:,i])

            vals.append(np.average(bhz_col))

            if np.average(bhz_col) > threshold:
                bhz_p[:,i] = 1e-20
            if np.average(bhn_col) > threshold:
                bhn_p[:,i] = 1e-20
            if np.average(bhe_col) > threshold:
                bhe_p[:,i] = 1e-20

    #Calculate envelopes in user defined frequency range
    yi_0 = find_nearest(f,freqmin)
    yi_1 = find_nearest(f,freqmax)

    bhz_sub_array = bhz_p[yi_0:yi_1,:]
    bhn_sub_array = bhn_p[yi_0:yi_1,:]
    bhe_sub_array = bhe_p[yi_0:yi_1,:]

    twindow_start = 0.0
    twindow_end = st[0].stats.npts * (1./st[0].stats.sampling_rate)
    utcwindow_start = st[0].stats.starttime
    utcwindow_end = st[0].stats.starttime + twindow_end
    fwindow_start = freqmin
    fwindow_end = freqmax

    bhz_envelope = np.average(bhz_sub_array,axis=0)
    max_env_bhz = np.max(bhz_envelope)
    bhz_envelope /= max_env_bhz
    t_wave = np.linspace(twindow_start,twindow_end,len(bhz_envelope))

    bhn_envelope = np.average(bhn_sub_array,axis=0)
    max_env_bhn = np.max(bhn_envelope)
    bhn_envelope /= max_env_bhn

    bhe_envelope = np.average(bhe_sub_array,axis=0)
    max_env_bhe = np.max(bhe_envelope)
    bhe_envelope /= max_env_bhe

    fout1 = open('{}_bhz_envelope.dat'.format(event_name),'w')
    fout2 = open('{}_bhn_envelope.dat'.format(event_name),'w')
    fout3 = open('{}_bhe_envelope.dat'.format(event_name),'w')

    fout1.write('#normalization factor {}\n'.format(max_env_bhz))
    fout1.write('#file start time {}\n'.format(utcwindow_start))
    fout1.write('#time window {} - {}\n'.format(str(utcwindow_start),str(utcwindow_end)))
    fout1.write('#freq window {} - {} Hz\n'.format(fwindow_start,fwindow_end))
    fout2.write('#normalization factor {}\n'.format(max_env_bhn))
    fout2.write('#file start time {}\n'.format(utcwindow_start))
    fout2.write('#time window {} - {}\n'.format(str(utcwindow_start),str(utcwindow_end)))
    fout2.write('#freq window {} - {} Hz\n'.format(fwindow_start,fwindow_end))
    fout3.write('#normalization factor {}\n'.format(max_env_bhe))
    fout3.write('#file start time {}\n'.format(utcwindow_start))
    fout3.write('#time window {} - {}\n'.format(str(utcwindow_start),str(utcwindow_end)))
    fout3.write('#freq window {} - {} Hz\n'.format(fwindow_start,fwindow_end))

    for i in range(0,len(t_wave)):
        fout1.write('{:10.10f} {:10.10f}\n'.format(t_wave[i],bhz_envelope[i]))
        fout2.write('{:10.10f} {:10.10f}\n'.format(t_wave[i],bhn_envelope[i]))
        fout3.write('{:10.10f} {:10.10f}\n'.format(t_wave[i],bhe_envelope[i]))

    fout1.close()
    fout2.close()
    fout3.close()
    
main()
